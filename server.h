//
// Created by Roy on 28/10/2018.
//

#ifndef LOADBALANCER_SERVER_H
#define LOADBALANCER_SERVER_H

using namespace std;

#include "task.h"
#include <queue>
#include <vector>
#include <list>
#include "distribution.h"

class Server {
private:
	queue<Task> Queue;
	double p; // geometric distribution
	int id;
public:
	//default ctor
	/*
	Server(){
		std::default_random_engine generator;
		std::geometric_distribution<int> distribution(0.3);
		int number = distribution(generator);
		this->p = number;
	}
	 */

	void pushTask(Task t); //should be called with a reference to a server
	int getServerQueueSize();
	void print();
	void setServerP(double p);
	void setServerID(int id);
	int getServerID();
	void completeTasks(std::geometric_distribution<int> dist_geom, std::default_random_engine eng);
	void clear();
};


#endif //LOADBALANCER_SERVER_H
