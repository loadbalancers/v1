/////////////////////////////// Includes: ///////////////////////////////

#include <iostream>
#include <random>
#include <fstream>
#include <time.h>
#include "loadBalancer.h"

/////////////////////////////// Constants: ///////////////////////////////

// definitions of time slice and servers
// final tests should take timeSlots = 1000000, numOfServers = 10

const int timeSlots = 10e3; // num of time iterations.
const int numOfServers = 10; // num of servers

// definitions of distributional parameters
// Condition: numOfServers*(1/p) >= lambda (homogeneous case)

const double lambda = 20.0; //
const double p = 0.5;
const int numOfFactors = 30; // width of sampling vector

string algorithms[] = {"JSQ", "JIQ", "PI"};

/////////////////////////////// Helper Functions: ///////////////////////////////

string getDate(){
	time_t rawtime;
	struct tm * timeinfo;
	int size = 80;
	char buffer [size];
	time (&rawtime);
	timeinfo = localtime (&rawtime);
	strftime (buffer,size,"%d_%m_%y_%H_%M_%S",timeinfo);
	return string(buffer);
}

/////////////////////////////// Main Function: ///////////////////////////////

int main() {
	LB_Type my_alg = JSQ;
	string filename = algorithms[my_alg] + "_" + to_string(numOfServers) + "_" + to_string(timeSlots) + "_" + getDate() + ".txt";
	ofstream meanTasksFile;
	meanTasksFile.open(filename);
	loadBalancer my_LB = loadBalancer(my_alg,numOfServers);
	my_LB.setHomogeneous(p);
	int currTasks; // amount of tasks in system
	uint64_t taskSum = 0;
	double meanTasks;
	double factors[numOfFactors] = {0.1, 0.2, 0.3, 0.4, 0.5, 0.55, 0.6, 0.65,
									0.7, 0.75, 0.8, 0.82, 0.84, 0.86, 0.88,
									0.9, 0.91, 0.92, 0.93, 0.94, 0.95, 0.96,
									0.965, 0.97, 0.975, 0.98, 0.985, 0.99,
									0.995, 0.998};

	unsigned seed = generateSeed();
    std::default_random_engine eng = generateEngine(seed);
	std::geometric_distribution<int> dist_geom(p); //homogenous

	for(int i=0; i < numOfFactors; i++){

		my_LB.clearServers();
		nextIdTask = 1;
        std::cout << i << endl;
        std::poisson_distribution<int> dist_pois(factors[i]*lambda); // called 30 times


        for(int j=0; j<timeSlots; j++){
			int tasks = dist_pois(eng);
			// start slot
            // find server to push to by the LB's algorithm
            int chosenServer = my_LB.doAlg();
            //cout << "chosen server is: " << chosenServer << endl;
            // push all tasks in LB queue to the chosen server
            my_LB.sendTasks(chosenServer, tasks);
            // all servers completing tasks according to their p
			my_LB.completeTasks(dist_geom, eng);
			// finish slot
			currTasks = my_LB.averageTasksInSystem();
			//cout << "avg tasks in system: " << currTasks << endl;
			taskSum += currTasks;
			// printing
			//my_LB.print();
			//cout << "--------------------------------------------------" << endl;
		}
		meanTasks = (double)taskSum/(double)timeSlots;
		meanTasksFile << factors[i] << " " << meanTasks << endl;
		taskSum = 0;
	}
	meanTasksFile.close();
	return 0;
}