//
// Created by Roy on 28/10/2018.
//

#include "server.h"
#include <iostream>

void Server::pushTask(Task t){
	this->Queue.push(t);
}

void Server::print(){
	queue<Task> printQ = Queue;
	int i=0;
	while (!printQ.empty()){
		std::cout << "Element number " << i << ": " << printQ.front().getId() << std::endl;
		printQ.pop();
		i++;
	}
}

void Server::setServerP(double p){
	this->p = p;
}

void Server::setServerID(int id){
	this->id = id;
}

int Server::getServerID(){
	return this->id ;
}

int Server::getServerQueueSize(){
	return Queue.size();
}

void Server::completeTasks(std::geometric_distribution<int> dist_geom, std::default_random_engine eng){
	int completedTasks = dist_geom(eng);
	//cout << "completed tasks: " << completedTasks << endl;
	int size = this->getServerQueueSize();
	if(completedTasks > size) // cant pop more than size of queue
		completedTasks = size;
	//cout << "server number " << this->getServerID() << ": popping " << completedTasks << " out of " << size << endl;
	for (int i = 0; i < completedTasks; i++)
		Queue.pop();
}

void Server::clear(){
	queue<Task> empty;
	swap(Queue,empty);
	/*
	int n = this->Queue.size();
	for (int i=0; i<n; i++){
		this->Queue.pop();
	}
	 */
}