//
// Created by Roy & Rachely on 28/10/2018.
//

#ifndef LOADBALANCER_LOADBALANCER_H
#define LOADBALANCER_LOADBALANCER_H
#include "server.h"
#include <set>

extern int nextIdTask;

enum LB_Type { JSQ, JIQ, PI };


class loadBalancer {
private:
	LB_Type alg;
	vector<Server> servers;
	set<int> empty_servers; // contains ID's of empty servers. only for JIQ, PI
public:
	loadBalancer(LB_Type t, int size){
		this->alg = t;
		this->servers = vector<Server>(size);
		int i = 0;
		for(vector<Server>::iterator it = servers.begin(); it!=servers.end(); it++){
			(*it).setServerID(i);
			empty_servers.insert(i);
			i++;
		}
	}

	void sendTasks(int srvPos, int amount);
	int doAlg();
	int doJSQ();
	int doJIQ();
	int doPI();
	void print();
	void completeTasks(std::geometric_distribution<int> dist_geom, std::default_random_engine eng);
	void setHomogeneous(double p);
	int averageTasksInSystem();
	void clearServers();
};



#endif //LOADBALANCER_LOADBALANCER_H
