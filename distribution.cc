//
// Created by Roy on 15/11/2018.
//

#include "distribution.h"

// distributional functions
int randomUniform(int a, int b){
	unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
	std::uniform_int_distribution<int> dist_uni(a,b); // range of uniformal distribution.
	std::default_random_engine e(seed);
	return dist_uni(e);
}

/*
std::geometric_distribution<int>* randomGeometric() {
}


	std::geometric_distribution<int> dist_geom(p); // p is rate of success. p is a double between [0,1]
	return dist_geom;
}
*/

// called once

unsigned generateSeed(){
	unsigned seed = std::chrono::steady_clock::now().time_since_epoch().count();
	return seed;
}

std::default_random_engine generateEngine(unsigned seed){
	std::default_random_engine engine(seed); // seed engine. must not be declared staticly
	return engine;
}

