//
// Created by Roy on 28/10/2018.
//
#include <iostream>
#include "loadBalancer.h"



int nextIdTask = 1; // each task will have its unique id

int loadBalancer::doAlg(){
	switch(this->alg){
		case JSQ : return doJSQ();
		case JIQ : return doJIQ();
		case PI : return doPI();
	}
}


void loadBalancer::sendTasks(int serverIndex, int numOfTasks){ // send to servers
	int old_size = this->servers.at(serverIndex).getServerQueueSize();
	for(int i=0; i<numOfTasks; i++){
		Task outTask = Task(nextIdTask);
		this->servers.at(serverIndex).pushTask(outTask);
		nextIdTask++;
	}
	if((this->alg == JIQ || this->alg == PI) && old_size == 0 && numOfTasks > 0)
		empty_servers.erase(serverIndex);
}

int loadBalancer::doJSQ(){
	int min = servers.at(0).getServerQueueSize(); // initialize first server as minimal
	int minId = 0;
	// first, we find the minimum value of tasks. We search in each server
	for(vector<Server>::iterator it = servers.begin(); it!=servers.end(); it++){
		int currServerQueueSize = (*it).getServerQueueSize();
		if(currServerQueueSize < min)
			min = currServerQueueSize;
	}
	vector<int> minServers;
	int currentId = 0; // build a vector containing the minimal servers, identified by their id(=index)
	for(vector<Server>::iterator it = servers.begin(); it!=servers.end(); it++){
		if((*it).getServerQueueSize() == min){
			minServers.push_back(currentId);
		}
		currentId++;
	}
	int minSize = minServers.size(); // number of servers with same minimal size
	//int minRand = randomUniform(0, minSize-1);
	//minId = minServers.at(minRand);
	minId = minServers.at(0);
	return minId;
}

//empty server set is being updated in functions: completeTasks (Adds to set), sendTasks (Removes from set)
int loadBalancer::doJIQ(){
	int range_uni, chosenServer, chosenSetIndex;
	if(empty_servers.size() == 0){ // if there are no empty servers, pick one randomly from all servers
		range_uni = this->servers.size();
		chosenServer = randomUniform(0,range_uni-1); //TODO: CHANGE THIS
		return chosenServer;
	}
	else{ // there are some empty servers, pick one randomly from empty servers
		range_uni = this->empty_servers.size();
		chosenSetIndex = randomUniform(0,range_uni-1); //TODO: CHANGE THIS
		set<int>::const_iterator it(empty_servers.begin());
		advance(it,chosenSetIndex);
		chosenServer = (*it);
		return chosenServer; // this is the index of chosen empty server
	}
}




int loadBalancer::doPI(){
	return 1;
}




void loadBalancer::print() {
	int i=0;
	for (vector<Server>::iterator it = servers.begin(); it != servers.end(); it++) {
		std::cout << "Server number " << i << ": " << std::endl;
		(*it).print();
		i++;
	}
}

void loadBalancer::setHomogeneous(double p) {
	for (vector<Server>::iterator it = servers.begin(); it != servers.end(); it++)
		(*it).setServerP(p);
	std::cout << "All Servers p: " << p << std::endl;
}

void loadBalancer::completeTasks(std::geometric_distribution<int> dist_geom, std::default_random_engine eng){
	for (vector<Server>::iterator it = servers.begin(); it != servers.end(); it++){
		//cout << "server number " << (*it).getServerID() << ": has old size " << (*it).getServerQueueSize() << endl;
		(*it).completeTasks(dist_geom, eng);
		if((this->alg == JIQ || this->alg == PI) && (*it).getServerQueueSize() == 0)
			this->empty_servers.insert((*it).getServerID());
		//cout << "server number " << (*it).getServerID() << ": has new size " << (*it).getServerQueueSize() << endl;
	}
}

int loadBalancer::averageTasksInSystem(){
	int taskSum = 0;
	for (vector<Server>::iterator it = servers.begin(); it != servers.end(); it++){
		taskSum += (*it).getServerQueueSize(); // number of tasks currently in the i-th server
	}
	return taskSum;
}

void loadBalancer::clearServers(){
	for (vector<Server>::iterator it = servers.begin(); it != servers.end(); it++)
		(*it).clear();
}