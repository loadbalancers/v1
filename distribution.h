//
// Created by Roy on 15/11/2018.
//

#include <iostream>
#include <random>
#include <chrono>

int randomUniform(int a, int b);
int randomGeometric(double p);



unsigned generateSeed();
std::default_random_engine generateEngine(unsigned seed);